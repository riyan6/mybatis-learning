package org.pingz.learning.session;

import org.pingz.learning.builder.xml.XmlConfigBuilder;
import org.pingz.learning.session.defaults.DefaultSqlSessionFactory;

import java.io.Reader;

/**
 * <p>SqlSessionFactory 构建类</p>
 * <p>通过build方法调用解析XML方法并最终得到SqlSessionFactory对象</p>
 */
public class SqlSessionFactoryBuilder {

    /**
     * 通过文件流构建 xml配置 再最终构建出 SqlSession工厂
     *
     * @param reader
     * @return
     */
    public SqlSessionFactory build(Reader reader) {
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    /**
     * 根据配置文件构建出 SqlSession工厂
     *
     * @param configuration
     * @return
     */
    public SqlSessionFactory build(Configuration configuration) {
        return new DefaultSqlSessionFactory(configuration);
    }

}
