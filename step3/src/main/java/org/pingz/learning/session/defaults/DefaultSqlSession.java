package org.pingz.learning.session.defaults;

import lombok.AllArgsConstructor;
import org.pingz.learning.mapping.MappedStatement;
import org.pingz.learning.session.Configuration;
import org.pingz.learning.session.SqlSession;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DefaultSqlSession implements SqlSession {

    private final Configuration configuration;

    @Override
    public <T> T selectOne(String statement) {
        return (T) ("【代理】%s".formatted(statement));
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        // 获取入参
        String params = "";
        if (parameter instanceof Object[] arr) {
            params = Arrays.stream(arr).map(Object::toString).collect(Collectors.joining(","));
        }
        // 获取执行sql
        MappedStatement mappedStatement = configuration.getMappedStatement(statement);
        String sql = mappedStatement.getSql().strip();
        // 返回结果
        return (T) ("【代理】方法：%s 入参：%s 执行SQL：%s ".formatted(statement, params, sql));
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return configuration.getMapper(type, this);
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }
}
