package org.pingz.learning.binding;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.ClassScanner;
import org.pingz.learning.session.SqlSession;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapperRegistry {

    private final Map<Class<?>, MapperProxyFactory<?>> mapperMap = new HashMap<>();

    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        MapperProxyFactory<T> proxyFactory = (MapperProxyFactory<T>) mapperMap.get(type);
        Assert.notNull(proxyFactory, "没有找到对应Type %s 的工厂".formatted(type));
        try {
            return proxyFactory.newInstance(sqlSession);
        } catch (Exception e) {
            throw new RuntimeException("获取Mapper实例失败", e);
        }
    }

    /**
     * 通过 Mapper类实例 注册Mapper
     *
     * @param type Mapper类实例
     * @param <T>  Object
     */
    public <T> void addMapper(Class<T> type) {
        if (type.isInterface()) {
            if (hasMapper(type)) {
                throw new RuntimeException("Type %s is already know to the MapperRegistry.".formatted(type));
            }
            mapperMap.put(type, new MapperProxyFactory<>(type));
        }
    }

    /**
     * 通过包名批量注册 Mapper
     *
     * @param packageName 包名
     */
    public void addMappers(String packageName) {
        // 扫描包下的所有类 进行注册
        Set<Class<?>> mappers = ClassScanner.scanPackage(packageName);
        mappers.forEach(this::addMapper);
    }

    /**
     * 判断是否存在某个 Mapper
     *
     * @param type Mapper类实例
     * @param <T>  Object
     * @return 代理Mapper
     */
    public <T> boolean hasMapper(Class<T> type) {
        return mapperMap.containsKey(type);
    }

}
