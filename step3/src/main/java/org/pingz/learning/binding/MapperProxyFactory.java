package org.pingz.learning.binding;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.pingz.learning.session.SqlSession;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public class MapperProxyFactory<T> {

    private final Class<T> mapperInterface;

    private Map<Method, MapperMethod> methodCache = new ConcurrentHashMap<>();

    public T newInstance(SqlSession sqlSession) {
        // 获取到被代理类
        MapperProxy<T> proxy = new MapperProxy<>(sqlSession, mapperInterface, methodCache);
        // 参数1：类加载器、参数2：被代理的类、参数3：实际执行类InvocationHandler
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[]{mapperInterface}, proxy);
    }

}
