package org.pingz.learning.builder;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.pingz.learning.session.Configuration;

/**
 * 配置基类
 */
@Getter
@RequiredArgsConstructor
public class BaseBuilder {

    /**
     * 配置
     */
    protected final Configuration configuration;

}
