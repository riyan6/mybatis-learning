package org.pingz.learning.builder.xml;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.pingz.learning.builder.BaseBuilder;
import org.pingz.learning.enums.SqlCommandType;
import org.pingz.learning.io.Resources;
import org.pingz.learning.mapping.MappedStatement;
import org.pingz.learning.session.Configuration;
import org.xml.sax.InputSource;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * XML 文件配置解析类
 */
public class XmlConfigBuilder extends BaseBuilder {

    /**
     * xml 文件根节点内容
     */
    private final Element root;

    public XmlConfigBuilder(Reader reader) {
        // 调用父类初始化Configuration，因为xml解析完毕需要通过config类将代理类注册进去
        super(new Configuration());
        // 2. dom4j 处理 xml
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(new InputSource(reader));
            root = document.getRootElement();
        } catch (Exception e) {
            throw new RuntimeException("初始化xml配置类时，xml文件解析失败 错误信息：%s".formatted(e.getMessage()));
        }
    }

    public Configuration parse() {
        try {
            mapperElement(root.element("mappers"));
        } catch (Exception e) {
            throw new RuntimeException("解析 Mapper.xml 内容至各个实体类失败 错误信息：%s".formatted(e.getMessage()));
        }
        return configuration;
    }

    /**
     * 将xml中的配置解析出来存储到对应的实体类中
     *
     * @param mappers
     * @throws Exception
     */
    private void mapperElement(Element mappers) throws Exception {
        // 提取出 mybatis-config.xml 中的每一个 mapper 文件关联信息
        List<Element> mapperList = mappers.elements("mapper");
        for (Element ele : mapperList) {
            String resource = ele.attributeValue("resource");
            Reader reader = Resources.getResourceAsReader(resource);
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(new InputSource(reader));
            Element root = document.getRootElement();

            // 命名空间
            String namespace = root.attributeValue("namespace");

            // select
            List<Element> selectNodes = root.elements("select");
            for (Element node : selectNodes) {
                String id = node.attributeValue("id");
                String parameterType = node.attributeValue("parameterType");
                String resultType = node.attributeValue("resultType");
                String sql = node.getText();

                // 替换 mapper.xml 中的SQL语句，将其对应的 #{paramName} 替换成SQL的占位符 "?"
                // 记录这条SQL的所有参数的Map对象 key参数index，value #{x}或${x}属性名中的x
                Map<Integer, String> parameter = new HashMap<>();
                Pattern pattern = Pattern.compile("(#\\{(.*?)})");
                // 解析SQL 提出出 matcher
                Matcher matcher = pattern.matcher(sql);
                // 遍历每一个条件
                for (int i = 1; matcher.find(); i++) {
                    // 条件值比如 #{name}
                    String g1 = matcher.group(1);
                    // 从g1中再次提取 比如 g1=#{name} g2=name
                    String g2 = matcher.group(2);
                    parameter.put(i, g2);
                    // 将sql中的属性名替换成 sql占位符
                    sql = sql.replace(g1, "?");
                }

                // mapper.methodName 全路径名
                String msId = namespace + "." + id;
                // xml标签名字比如 select
                String nodeName = node.getName();
                // 提取出标准枚举
                SqlCommandType sqlCommandType = SqlCommandType.valueOf(nodeName.toUpperCase(Locale.ENGLISH));

                MappedStatement mappedStatement = new MappedStatement.Builder(configuration, msId, sqlCommandType,
                        parameterType, resultType, sql, parameter).build();
                // 添加解析SQL
                configuration.addMappedStatement(mappedStatement);
            }


            // 注册代理注册器
            configuration.addMapper(Resources.classForName(namespace));
        }
    }
}
