package org.pingz.learning.mapper;

public interface UserMapper {

    String selectNameByUserId(Long userId);

}
