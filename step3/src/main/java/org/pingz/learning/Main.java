package org.pingz.learning;

import org.pingz.learning.io.Resources;
import org.pingz.learning.mapper.UserMapper;
import org.pingz.learning.session.SqlSession;
import org.pingz.learning.session.SqlSessionFactory;
import org.pingz.learning.session.SqlSessionFactoryBuilder;

import java.io.Reader;

public class Main {
    public static void main(String[] args) throws Exception {
        // 1.从SqlSessionFactory中获取SqlSession
        Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 2.获取映射器对象
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        // 3.测试
        String result = mapper.selectNameByUserId(10066883L);
        System.out.println(result);
    }
}