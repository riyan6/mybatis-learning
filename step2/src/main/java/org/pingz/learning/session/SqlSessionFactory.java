package org.pingz.learning.session;

public interface SqlSessionFactory {

    /**
     * 开启 session
     *
     * @return sqlSession
     */
    SqlSession openSession();

}
