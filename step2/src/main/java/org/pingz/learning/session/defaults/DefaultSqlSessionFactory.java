package org.pingz.learning.session.defaults;

import lombok.AllArgsConstructor;
import org.pingz.learning.binding.MapperRegistry;
import org.pingz.learning.session.SqlSession;
import org.pingz.learning.session.SqlSessionFactory;

@AllArgsConstructor
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private final MapperRegistry mapperRegistry;

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(mapperRegistry);
    }
}
