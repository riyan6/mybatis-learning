package org.pingz.learning.session.defaults;

import lombok.AllArgsConstructor;
import org.pingz.learning.binding.MapperRegistry;
import org.pingz.learning.session.SqlSession;

@AllArgsConstructor
public class DefaultSqlSession implements SqlSession {

    private final MapperRegistry mapperRegistry;

    @Override
    public <T> T selectOne(String statement) {
        return (T) ("【代理】%s".formatted(statement));
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        return (T) ("【代理】方法：%s 入参：%s".formatted(statement, parameter.toString()));
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return mapperRegistry.getMapper(type, this);
    }
}
