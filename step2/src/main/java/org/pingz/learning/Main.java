package org.pingz.learning;

import org.pingz.learning.binding.MapperRegistry;
import org.pingz.learning.mapper.UserMapper;
import org.pingz.learning.session.SqlSession;
import org.pingz.learning.session.SqlSessionFactory;
import org.pingz.learning.session.defaults.DefaultSqlSessionFactory;

public class Main {

    public static void main(String[] args) {
        // 1.注册 Mapper
        MapperRegistry registry = new MapperRegistry();
        registry.addMappers("org.pingz.learning.mapper");

        // 2.从 SqlSessionFactory 获取 session
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(registry);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3.获取映射器对象
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        // 4.测试
        String result = mapper.selectNameByAddress("changsha");
        System.out.println(result);
    }

}
