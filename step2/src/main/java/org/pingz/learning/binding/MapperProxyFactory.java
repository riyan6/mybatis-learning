package org.pingz.learning.binding;

import lombok.AllArgsConstructor;
import org.pingz.learning.session.SqlSession;

import java.lang.reflect.Proxy;

@AllArgsConstructor
public class MapperProxyFactory<T> {

    private final Class<T> mapperInterface;

    public T newInstance(SqlSession sqlSession) {
        // 获取到被代理类
        MapperProxy<T> proxy = new MapperProxy<>(sqlSession, mapperInterface);
        // 参数1：类加载器、参数2：被代理的类、参数3：实际执行类InvocationHandler
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[]{mapperInterface}, proxy);
    }

}
